///*
//#pragma once
#ifndef SERIAL_2_SERIAL_H
#define SERIAL_2_SERIAL_H

#include <Arduino.h>

namespace Serial2SerialSpace
{
  class Serial2SerialClass
  {
    public:
      //enum Status {Send, Recv, Wait};
      Serial2SerialClass();
      void begin(long speed);
      void end();
      bool hasMessage();
      bool read(char* buf, uint8_t& bufSize);
      void clear();
      //bool flush();
      bool send(const char* buf, uint8_t msgSize);
      bool send(const uint8_t* buf, uint8_t msgSize);
      bool isBigEndian();
      //int htoni(int hostInt);
      //int ntohi(int netInt);
  
    private:
      unsigned long _timer;
      uint8_t _state;
      //Status _stat;
      //void _sync();
      bool _bufferWait(unsigned long);
      bool _sync(uint8_t);
      bool _check();
      bool _readyFailure();
      bool _readySuccess();
  };
}

extern Serial2SerialSpace::Serial2SerialClass Serial2Serial;

#endif /* SERIAL_2_SERIAL_H */
//*/
