///*
#include <Arduino.h>
#include "Serial2Serial.h"

//#define _DEBUG

namespace Serial2SerialSpace
{
  enum State
  {
    Ready,
    Incoming,
    Outgoing,
    Reading,
    Sending,
    Terminated,
  };
  
  enum Signal
  {
    Ack,
    Busy,
    Start,
    End
  };
  
  Serial2SerialClass::Serial2SerialClass()
  {
    _state = State::Terminated;
  }
  
  void Serial2SerialClass::begin(long speed)
  {
    if (_state != State::Terminated) return;
    Serial.begin(speed);
    #ifdef _DEBUG
      Serial.println("Serial2Serial Started");
    #endif
    _state = State::Ready;
  }
  
  void Serial2SerialClass::end()
  {
    if (_state == State::Terminated) return;
    Serial.end();
    _state = State::Terminated;
  }
  
  bool Serial2SerialClass::hasMessage()
  {
    #ifdef _DEBUG_STATUS
      switch (_state)
      {
        case State::Ready:
          Serial.println("Ready");
          break;
        case State::Incoming:
          Serial.println("Incoming");
          break;
        case State::Outgoing:
          Serial.println("Outgoing");
          break;
        case State::Reading:
          Serial.println("Reading");
          break;
        case State::Sending:
          Serial.println("Sending");
          break;
        case State::Terminated:
          Serial.println("Terminated");
          break;
        default:
          Serial.println("Unknown");
      }
    #endif
    if (_state == State::Terminated) return false;
    return _check();
  }
  
  bool Serial2SerialClass::read(char* buf, uint8_t& bufSize)
  {
    if (_state == State::Terminated || bufSize >= 256) return false;

    #ifdef _DEBUG
      Serial.println("Reading");
    #endif

    //Check if a message is waiting and set to reading if there is
    if (_check())
    {
      _state = State::Reading;
    }
    //Otherwise return fail
    else
    {
      bufSize = 0;
      return false;
    }

    #ifdef _DEBUG
      Serial.println("Post Check Reading, Should Be Waiting 10 Seconds");
    #endif

    //Wait to receive the size of the data to be sent, fail if times out
    uint8_t msgSize = 0;
    if (!_bufferWait(3000))
    {
      bufSize = 0;
      return _readyFailure();
    }
    //Otherwise set size of message
    else
    {
      #ifdef _DEBUG
        int input = Serial.read();
        if (isDigit(input))
        {
          String str = "";
          str += (char)input;
          msgSize = str.toInt();
        }
      #else
        msgSize = Serial.read();
      #endif
    }

    //Send busy to sender if buffer too small for message, then fail
    if (msgSize > bufSize)
    {
      #ifdef _DEBUG
        Serial.println('B');
      #else
        Serial.write(Signal::Busy);
      #endif
      bufSize = 0;
      return _readyFailure();
    }
    //Acknowledge msg size was received and is good
    else
    {
      bufSize = msgSize;
      #ifdef _DEBUG
        Serial.println('A');
      #else
        Serial.write(Signal::Ack);
      #endif
    }
    
    #ifdef _DEBUG
      Serial.print("Message size is ");
      Serial.println(msgSize);
    #endif

    //Read in message and store into user given buffer
    while (msgSize > 0)
    {
      #ifdef _DEBUG
        Serial.println("Reading up to 32 bytes");
      #endif
      for (int i = 0; i < 32 && msgSize > 0; i++)
      {
        //Wait until next byte is received or fail if timeout occurs
        if (!_bufferWait(3000))
        {
          #ifdef _DEBUG
            Serial.println("Reading in for loop timeout has occured");
          #endif
          bufSize = 0;
          return _readyFailure();
        }

        //Read data into next byte of buffer and increment
        *buf = Serial.read();
        
        #ifdef _DEBUG
          Serial.print("Character ");
          Serial.print(*buf);
          Serial.println(" read");
        #endif
        
        buf++;
        msgSize--;
      }

      //Acknowledge that all data for that transmission burst was received
      #ifdef _DEBUG
        Serial.println('A');
      #else
        Serial.write(Signal::Ack);
      #endif
    }

    //Wait for the stop signal or fail if timeout
    #ifdef _DEBUG
      if (!_bufferWait(3000) || Serial.peek() != 'E')
    #else
      if (!_bufferWait(3000) || Serial.peek() != Signal::End)
    #endif
    {
      Serial.read();
      bufSize = 0;
      return _readyFailure();
    }
    Serial.read();
    return _readySuccess();
  }

  void Serial2SerialClass::clear()
  {
    while (Serial.available()) Serial.read();
  }

  /*
  bool Serial2SerialClass::flush()
  {
    
  }
  */
  
  bool Serial2SerialClass::send(const char* buf, uint8_t msgSize)
  {
    if (_state == State::Terminated || msgSize >= 256) return false;
    
    //Try synchronizing with counterpart if not return failure
    if (!_sync(10)) return false;
    else _state = State::Sending;

    //Write out size of message and wait for a reply
    #ifdef _DEBUG
      Serial.println(msgSize);
    #else
      Serial.write(msgSize);
    #endif
    
    //No reply in time or acknowledgement wasn't given so fail
    //Note: If receiver buffer is too small anknowledgement will fail
    #ifdef _DEBUG
      if (!_bufferWait(3000) || Serial.peek() != 'A') return _readyFailure();
    #else
      if (!_bufferWait(3000) || Serial.peek() != Signal::Ack) return _readyFailure();
    #endif
    //Otherwise Ack was received so consume
    else Serial.read();

    //Write message to receiver
    while (msgSize > 0)
    {
      //Send up to 32 bytes of message to receiver
      if (msgSize > 32)
      {
        Serial.write(buf, 32);
        buf += 32;
        msgSize -= 32;
      }
      else
      {
        Serial.write(buf, msgSize);
        msgSize = 0;
      }

      //Wait for an acknowledgement until receipt or timeout
      #ifdef _DEBUG
        if (!_bufferWait(3000) || Serial.peek() != 'A') return _readyFailure();
      #else
        if (!_bufferWait(3000) || Serial.peek() != Signal::Ack) return _readyFailure();
      #endif
      //Otherwise Ack was received so consume
      else Serial.read();
    }

    //All information sent and aknowledged so send stop signal and finish
    #ifdef _DEBUG
      Serial.println('E');
    #else
      Serial.write(Signal::End);
    #endif
    return _readySuccess();
  }

  bool Serial2SerialClass::send(const uint8_t* buf, uint8_t msgSize)
  {
    send((const char*)buf, msgSize);
  }

  bool Serial2SerialClass::isBigEndian()
  {
    int test = 17;
    if (*((char*)&test) == 17)
    {
      //Little Endian (17 was in first byte)
      return false;
    }
    else
    {
      //Big Endian (17 was not in first byte)
      return true;
    }
  }
    
  //int Serial2SerialClass::htoni(int hostInt);
  //int Serial2SerialClass::ntohi(int netInt);

  //Wait 
  bool Serial2SerialClass::_bufferWait(unsigned long waittime)
  {
    #ifdef _DEBUG
      waittime = 10000;
    #endif
    unsigned long startTime = millis();
    while ((unsigned long)(millis() - startTime) <= waittime && Serial.available() == 0)
    {
      yield();
    }

    if (Serial.available() == 0) return false;
    else return true;
  }
  
  //Synchronize communications with other end to start sending data
  bool Serial2SerialClass::_sync(uint8_t tryCount)
  {
    //If data is in buffer or com is busy fail
    if (Serial.available() > 0 || _state != State::Ready) return false;
  
    //Try to sync with counterpart up to inclusively tryCount
    for (int i = 0; i <= tryCount; i++)
    {
      //Standard Binary Backoff Algorithm, with slotTime of 1 microsecond
      unsigned long waitTime = ((unsigned long)(1 << random(i+1)) - 1); //2^c -1
      unsigned long startTime = micros();
      while ((unsigned long)(micros() - startTime) <= waitTime)
      {
        yield();
      }

      //Send start send command
      #ifdef _DEBUG
        Serial.println('S');
      #else
        Serial.write(Signal::Start);
      #endif
      
      //Wait until a reply is received or timeout occurs
      if (!_bufferWait(3000)) return false;
      //Check incoming byte and verify it's correct
      else if (Serial.available() == 1)
      {
        #ifdef _DEBUG
          if (Serial.peek() == 'A')
        #else
          if (Serial.peek() == Signal::Ack)
        #endif
        {
          Serial.read();
          _state = State::Outgoing;
          return true;
        }
        //Collision has occured, flush buffer and retry
        #ifdef _DEBUG
          else if (Serial.peek() == 'S')
        #else
          else if (Serial.peek() == Signal::Start)
        #endif
        {
          Serial.read();
          continue;
        }
        //Other data was received so fail
        else return false;
      }
      //Multiple bytes of data received so fail
      else return false;
    }
    
    //Try count has been reached so backoff has failed
    return false;
  }
  
  bool Serial2SerialClass::_check()
  {
    //If data was already checked, succeed
    if (_state == State::Incoming) return true;     
    //If no data is available or system is not ready, fail
    else if (Serial.available() == 0 || _state != State::Ready) return false;
    //More than 1 byte was received so flush and fail
    else if (Serial.available() > 1)
    {
      #ifdef _DEBUG
        Serial.println("Incoming Data Too Large");
        Serial.print("Size of data is ");
        Serial.println(Serial.available());
      #endif
      
      clear();
      return false;
    }
   
    #ifdef _DEBUG
      Serial.println("Incoming");
    #endif

    //System is ready and check if start packet was received
    #ifdef _DEBUG
      if (Serial.peek() == 'S')
    #else
      if (Serial.peek() == Signal::Start)
    #endif
    {
      Serial.read();
      #ifdef _DEBUG
        Serial.println('A');
      #else
        Serial.write(Signal::Ack);
      #endif
      _state = State::Incoming;
      return true;
    }
    //Else consume packet and fail
    else
    {
      Serial.read();
      return false;
    }
  }

  bool Serial2SerialClass::_readyFailure()
  {
    _state = State::Ready;
    return false;
  }

  bool Serial2SerialClass::_readySuccess()
  {
    _state = State::Ready;
    return true;
  }
}

Serial2SerialSpace::Serial2SerialClass Serial2Serial;
//*\
